//
//  GameScene.swift
//  brazucas
//
//  Created by zJorge on 07/10/14.
//  Copyright (c) 2014 Zoada Apps Factory. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    override func didMoveToView(view: SKView) {
        let origen = CGPoint(x: 100, y: 100)
        agregaSolidoEnPunto(origen,ancho:20,alto:200)
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            println("\(location.x.description),\(location.y.description)")
            //NSLog(location.y.description)
            
            let balon = SKSpriteNode(imageNamed:"brazuca_mini.png")
            
            balon.xScale = 0.50
            balon.yScale = 0.50
            balon.position = location
            
            balon.physicsBody = SKPhysicsBody(circleOfRadius:40)
            balon.physicsBody.dynamic = true
            
            balon.zPosition = 10
            
            self.addChild(balon)
        }
    }
   
    func agregaSolidoEnPunto(elPunto: CGPoint, ancho: CGFloat, alto: CGFloat) {
        let tamanio = CGSize(width: ancho, height: alto)
        let laPared = SKNode()
        laPared.position = CGPoint( x: elPunto.x + tamanio.width * 0.5,
            y: elPunto.y - tamanio.height * 0.5)
        laPared.physicsBody = SKPhysicsBody(rectangleOfSize: tamanio)
        laPared.physicsBody.dynamic = false
        laPared.physicsBody.collisionBitMask = 0
        
        self.addChild(laPared)
    }

    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
